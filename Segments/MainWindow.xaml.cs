﻿using DataLayer;
using DataLayer.Geometry;
using EFRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Segments
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Segment[] ArrSegments { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        //Создать отрезки и сохранить в БД
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //Очистка окна вывода
            grid1.Children.Clear();
            //Создание множества отрезков
            ArrSegments = MathFactory.CreateSegments( Convert.ToInt32(textBox.Text), 400, 0);
            
            //Сохранение в БД
            if ((bool)checkBoxSaveDB.IsChecked)
            {
                SegmentsRepository segRep = new SegmentsRepository();
                foreach (Segment item in ArrSegments)
                    segRep.Add(item);               
            }
            
            //Вывод отрезков в графику
            foreach (Segment item in ArrSegments)
            {
                Line ln = new Line();                
                ln.X1 = item.Start.XPos;
                ln.Y1 = item.Start.YPos;
                ln.X2 = item.End.XPos;
                ln.Y2 = item.End.YPos;

                if (item.IsSplitted)
                    ln.Stroke = Brushes.Red;
                else
                    ln.Stroke = Brushes.Blue;

                grid1.Children.Add(ln);                
            }

        }
              

        //Разбить отрезки и сохранить в БД
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            //Очистка окна вывода
            grid1.Children.Clear();

            Segment[] temp = ArrSegments;
            MathFactory.Gap = Convert.ToInt32(textBox_Gap.Text);
            MathFactory.FindIntersections(ref temp);
            ArrSegments = temp;

            //Сохранение в БД
            if ((bool)checkBoxSaveDB.IsChecked)
            {
                SplitSegmentsRepository segRep = new SplitSegmentsRepository();
                foreach (Segment item in ArrSegments)
                    if (item.IsSplitted)
                        segRep.Add(item);
            }
            
            
            //Вывод отрезков в графику
            foreach (Segment item in ArrSegments)
            {
                Line ln = new Line();
                ln.X1 = item.Start.XPos;
                ln.Y1 = item.Start.YPos;
                ln.X2 = item.End.XPos;
                ln.Y2 = item.End.YPos;

                if (item.IsSplitted)
                    ln.Stroke = Brushes.Red;
                else
                    ln.Stroke = Brushes.Blue;

                grid1.Children.Add(ln);
            }
        }

        //Проверка вводимого текста на число
        private void textBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {            
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    
    }
}
