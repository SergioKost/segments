﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataLayer.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Geometry.Tests
{
    [TestClass()]
    public class MathFactoryTests
    {
        [TestMethod()]
        public void FindIntersectionsTest()
        {
            //Входные данные
            Segment[] arrSegm = new Segment[2];
            arrSegm[0] = new Segment() { Start = new Point(0, 0), End = new Point(1, 1) };
            arrSegm[1] = new Segment() { Start = new Point(0, 1), End = new Point(1, 0) };
            //Ищем пересечения
            MathFactory.FindIntersections(ref arrSegm);
            //Проверяем, сколько стало отрезков (должно стать 3, т.к. 1 пересечение)
            Assert.AreEqual(3, arrSegm.Length);
        }
    }
}