﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    /// <summary>
    /// Отрезок
    /// </summary>
    public class Segment
    {
        /// <summary>
        /// Начало отрезка
        /// </summary>
        public Point Start { get; set; }
        /// <summary>
        /// Конец отрезка
        /// </summary>
        public Point End { get; set; }
        //Коэффициенты уравнения прямой, проходящей через отрезок
        public float A { get; set; }
        public float B { get; set; }
        public float C { get; set; }
        //Показывает, это "осколок" от разбиения другого отрезка или нет
        public bool IsSplitted { get; set; }

        public Segment(){
            Start = new Point();
            End = new Point();
        }

        /// <summary>
        /// Возвращает угол наклона к горизонтали в радианах от 0 до П/2
        /// </summary>
        /// <returns></returns>
        public double GetAngle(){

            return (B!= 0) ? Math.Abs(Math.Atan(A / B)) : 90;
        }
        //
    }
}
