﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    /// <summary>
    /// Пересечение двух отрезков
    /// </summary>
    public class Intersection
    {
        /// <summary>
        /// Отрезки, участвующие в пересечении
        /// </summary>
        public Segment[] Segments { get; set; }
        /// <summary>
        /// Точка пересечения
        /// </summary>
        public Point IntPoint { get; set; }

        public Intersection()
        {
            Segments = new Segment[2];
        }
    }
}
