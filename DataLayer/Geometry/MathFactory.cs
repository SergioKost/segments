﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Geometry
{
    //Класс, содержащий вспомогательные функции
    public static class MathFactory
    {
        //Точность, по которой сравниваются double (количество знаков после запятой)
        public const int prec = 7;
        
        //Величина зазора при разбиении
        public static int Gap { get; set; }
        
        /// <summary>
        /// Автоматическая генерация множества отрезков случайным методом
        /// </summary>
        /// <param name="count">Количество отрезков</param>
        /// <param name="limitPlus">Максимальное значение координаты</param>
        /// <param name="limitMinus">Минимальное значение координаты</param>
        /// <returns></returns>
        public static Segment[] CreateSegments(int count, int limitPlus, int limitMinus)
        {
            Segment[] result = new Segment[count];
            Random rand = new Random();
            for (int i = 0; i < count; i++)
            {
                int xs = rand.Next(limitMinus, limitPlus);
                int ys = rand.Next(limitMinus, limitPlus);
                int xe = rand.Next(limitMinus, limitPlus);
                int ye = rand.Next(limitMinus, limitPlus);
                result[i] = new Segment()
                {
                    Start = new Point(xs, ys),
                    End = new Point(xe, ye)
                };
            }
            return result;
        }

        /// <summary>
        /// Ищем пересечения в заданном множестве отрезков
        /// </summary>
        /// <param name="arrSegments">Массив отрезков для поиска</param>
        /// <returns></returns>
        public static void FindIntersections(ref Segment[] arrSegments)
        {
            //Метод перебора      
            for (int i = 0; i < arrSegments.Length - 1; i++)
            {
                for (int j = i + 1; j < arrSegments.Length; j++)
                {
                    Intersection intr = FindIntersect(ref arrSegments[i], ref arrSegments[j]);
                    if (intr != null)
                        //разбиваем 
                        SplitSegments(ref arrSegments, intr);
                }
            }
            //Удаляем нулевые значение массива
            arrSegments = arrSegments.Where(x => x != null).ToArray();
        }

        /// <summary>
        /// Возвращает точку пресечения в одной паре отрезков или null, если пересечений нет
        /// </summary>
        /// <param name="segm1">Отрезок 1</param>
        /// <param name="segm2">Отрезок 2</param>
        /// <returns></returns>
        public static Intersection FindIntersect(ref Segment segm1, ref Segment segm2)
        {
            //Максимальные значения координат отрезков
            float maxS1X = Math.Max(segm1.Start.XPos, segm1.End.XPos);
            float maxS1Y = Math.Max(segm1.Start.YPos, segm1.End.YPos);

            float maxS2X = Math.Max(segm2.Start.XPos, segm2.End.XPos);
            float maxS2Y = Math.Max(segm2.Start.YPos, segm2.End.YPos);

            //Минимальные значения координат отрезков
            float minS1X = Math.Min(segm1.Start.XPos, segm1.End.XPos);
            float minS1Y = Math.Min(segm1.Start.YPos, segm1.End.YPos);

            float minS2X = Math.Min(segm2.Start.XPos, segm2.End.XPos);
            float minS2Y = Math.Min(segm2.Start.YPos, segm2.End.YPos);

            ////условие, когда точно не пересекаются
            if (minS1X > maxS2X || maxS1X < minS2X || minS1Y > maxS2Y || maxS1Y < minS2Y)
            {
                //случай, когда имеют общую вершину - ?
                return null;
            }

            //определяем уравнение прямой 1 отрезка
            float a1, b1, c1;
            KoefLine(segm1, out a1, out b1, out c1);

            //определяем уравнение прямой 2 отрезка
            float a2, b2, c2;
            KoefLine(segm2, out a2, out b2, out c2);

            //Точка пересечения двух прямых
            float d, x, y;
            d = a1 * b2 - a2 * b1;
            if (!DoubleEq(d, 0, prec)) //прямые не параллельны
            {
                float dx = -c1 * b2 + b1 * c2;
                float dy = -a1 * c2 + c1 * a2;
                x = dx / d;
                y = dy / d;
            }
            else
            {
                return null;
            }

            //Определить, принадлежит ли точка пересечения прямых обоим отрезкам
            //Если точка пересечения совпадает с концом одного из отрезков - то это не считается пересечением
            //все расчеты ведутся условно с учетом точности prec
            bool t1 = (DoubleGreatOrEq(x, minS1X, prec)
                && DoubleLessOrEq(x, maxS1X, prec)
                && DoubleGreatOrEq(y, minS1Y, prec)
                && DoubleLessOrEq(y, maxS1Y, prec));
            bool t2 = (DoubleGreatOrEq(x, minS2X, prec)
                && DoubleLessOrEq(x, maxS2X, prec)
                && DoubleGreatOrEq(y, minS2Y, prec)
                && DoubleLessOrEq(y, maxS2Y, prec));
            if (t1 && t2) //Принадлежит
            {
                //Проверка, совпадает ли точка пересечения с концами первого отрезка
                if (DoubleEq(x, segm1.Start.XPos, prec) && DoubleEq(y, segm1.Start.YPos, prec))
                    return null;
                if (DoubleEq(x, segm1.End.XPos, prec) && DoubleEq(y, segm1.End.YPos, prec))
                    return null;

                //Проверка, совпадает ли точка пересечения с концами второго отрезка
                if (DoubleEq(x, segm2.Start.XPos, prec) && DoubleEq(y, segm2.Start.YPos, prec))
                    return null;
                if (DoubleEq(x, segm2.End.XPos, prec) && DoubleEq(y, segm2.End.YPos, prec))
                    return null;

                Point pI = new Point(x, y);
                Intersection result = new Intersection();
                result.IntPoint = pI;
                result.Segments[0] = segm1;
                result.Segments[0].A = a1;
                result.Segments[0].B = b1;
                result.Segments[0].C = c1;
                result.Segments[1] = segm2;
                result.Segments[1].A = a2;
                result.Segments[1].B = b2;
                result.Segments[1].C = c2;
                return result;
            }
            else // Не принадлежит
                return null;
        }

        /// <summary>
        /// Функция проверки на равенство двух double
        /// </summary>
        /// <param name="a">Парам 1</param>
        /// <param name="b">Парам 2</param>
        /// <param name="precision">Точность</param>
        /// <returns></returns>
        public static bool DoubleEq(double a, double b, int precision)
        {
            a = Math.Round(a, precision);
            b = Math.Round(b, precision);
            return a.Equals(b);
        }
        /// <summary>
        /// Функция проверки на больше либо равно двух double
        /// </summary>
        /// <param name="a">Парам 1</param>
        /// <param name="b">Парам 2</param>
        /// <param name="precision">Точность</param>
        /// <returns></returns>
        public static bool DoubleGreatOrEq(double a, double b, int precision)
        {
            a = Math.Round(a, precision);
            b = Math.Round(b, precision);
            return a >= b;
        }
        /// <summary>
        /// Функция проверки на меньше либо равно двух double
        /// </summary>
        /// <param name="a">Парам 1</param>
        /// <param name="b">Парам 2</param>
        /// <param name="precision">Точность</param>
        /// <returns></returns>
        public static bool DoubleLessOrEq(double a, double b, int precision)
        {
            a = Math.Round(a, precision);
            b = Math.Round(b, precision);
            return a <= b;
        }
        /// <summary>
        /// Метод, вычисляющий значения коэффициентов уравнения прямой
        /// </summary>
        /// <param name="segm"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public static void KoefLine(Segment segm, out float a, out float b, out float c)
        {
            a = segm.Start.YPos - segm.End.YPos;
            b = segm.End.XPos - segm.Start.XPos;
            c = (segm.Start.XPos * segm.End.YPos) - (segm.Start.YPos * segm.End.XPos);
        }

        /// <summary>
        /// Функция, разбивающая отрезка по найденному пересечению
        /// </summary>
        /// <param name="arrInSegm">Исходный массив отрезков</param>
        /// <param name="ints">Пересечение</param>
        public static void SplitSegments(ref Segment[] arrInSegm, Intersection ints)
        {
            if (arrInSegm != null && arrInSegm.Length > 1 && ints != null)
            {
                //Определить отрезок, кот. необходимо разбить (по коэф. - A/B)
                Segment spltSegm;
                //Вычислаем углы наклона
                int index;
                if (ints.Segments[1].GetAngle() < ints.Segments[0].GetAngle())
                    index = Array.FindIndex(arrInSegm, x => (x == ints.Segments[1]));               
                else
                    index = Array.FindIndex(arrInSegm, x => (x == ints.Segments[0]));

                spltSegm = arrInSegm[index];
                
                //Вертикальный отрезок никогда не выступит в качестве разбиваемого
                //поэтому используем приращение по Х
                //dx всегда неотрицательное, т.к. значение Cos (GetAngle()) неотрицательное
                float dx = Gap * (float)Math.Cos(spltSegm.GetAngle()) / 2;
                Point gapPoint1 = new Point();
                gapPoint1.XPos = ints.IntPoint.XPos - dx;
                gapPoint1.YPos = -(spltSegm.A / spltSegm.B) * gapPoint1.XPos - (spltSegm.C / spltSegm.B);
                Point gapPoint2 = new Point();
                gapPoint2.XPos = ints.IntPoint.XPos + dx;
                gapPoint2.YPos = -(spltSegm.A / spltSegm.B) * gapPoint2.XPos - (spltSegm.C / spltSegm.B);

                //Случай, когда spltSegm.Start.XPos левее spltSegm.End.XPos
                if (spltSegm.Start.XPos < spltSegm.End.XPos)
                {
                    float t1 = gapPoint1.XPos - spltSegm.Start.XPos;
                    float t2 = spltSegm.End.XPos - gapPoint2.XPos;

                    if (t1 < 0 || t2 < 0) //точка gapPoint1 или gapPoint2 выходит за границы отрезка
                        if (t1 < 0 && t2 < 0 ) //Разбиваемый отрезок вырождается в точку, т.к. полностью перекрывается зазором
                            spltSegm = null;
                        else
                            if (t1 < 0) //Усечение исходного отрезка слева
                            {
                                spltSegm.Start = gapPoint2;
                                spltSegm.IsSplitted = true;
                            }
                            else //Усечение исходного отрезка справа
                            {
                                spltSegm.End = gapPoint1;
                                spltSegm.IsSplitted = true;
                            }
                    else //точка gapPoint1 или gapPoint2 лежат внутри отрезка
                    {
                        Segment newSegm = new Segment(); //Создаем новый отрезок
                        newSegm.Start = gapPoint2;
                        newSegm.End = spltSegm.End;
                        newSegm.IsSplitted = true;

                        spltSegm.End = gapPoint1; //Перебиваем конец исходного отрезка
                        spltSegm.IsSplitted = true;
                        //добавляем новый отрезок в массив
                        Array.Resize(ref arrInSegm, arrInSegm.Length + 1);
                        arrInSegm[arrInSegm.Length - 1] = newSegm;
                    }
                }

                else //Случай, когда spltSegm.Start.XPos правее spltSegm.End.XPos
                {
                    float t1 = gapPoint1.XPos - spltSegm.End.XPos;
                    float t2 = spltSegm.Start.XPos - gapPoint2.XPos;

                    if (t1 < 0 || t2 < 0) //точка gapPoint1 или gapPoint2 выходит за границы отрезка
                        if (t1 < 0 && t2 < 0) //Разбиваемый отрезок вырождается в точку, т.к. полностью перекрывается зазором
                            spltSegm = null;
                        else
                            if (t1 < 0) //Усечение исходного отрезка слева
                            {
                                spltSegm.End = gapPoint2;
                                spltSegm.IsSplitted = true;
                            }
                            else //Усечение исходного отрезка справа
                            {
                                spltSegm.Start = gapPoint1;
                                spltSegm.IsSplitted = true;
                            }
                    else //точка gapPoint1 или gapPoint2 лежат внутри отрезка
                    {
                        Segment newSegm = new Segment(); //Создаем новый отрезок
                        newSegm.Start = gapPoint2;
                        newSegm.End = spltSegm.Start;
                        newSegm.IsSplitted = true;

                        spltSegm.Start = gapPoint1; //Перебиваем конец исходного отрезка
                        spltSegm.IsSplitted = true;
                        //добавляем новый отрезок в массив
                        Array.Resize(ref arrInSegm, arrInSegm.Length + 1);
                        arrInSegm[arrInSegm.Length - 1] = newSegm;
                    }
                }
            }
        }
    }
}
