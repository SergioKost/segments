﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    /// <summary>
    /// Точка с двумя координатами
    /// </summary>
    public class Point
    {
        public float XPos { get; set; }
        public float YPos { get; set; }

        public Point(float x, float y)
        {
            XPos = x;
            YPos = y;
        }
        public Point() { }
    }
}
