﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFRepository.Repository
{
    //Интерфейс для реализации репозиториев работы с БД
    public interface IRepository<T> where T: class
    {
        //выбрать все сущности
        IEnumerable<T> SelectAll();
        //Выбрать одну сущность по id
        T Select(Int64 id);
        //Обновить сущность
        bool Update(T entity);
        //Добавить сущность в БД
        bool Add(T entity);
        //Удалить сущность из БД по id
        bool Delete(Int64 id);
    }
}
