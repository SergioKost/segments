﻿using DataLayer;
using EFRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFRepository.Repository
{
    //Класс репозитория для работы с базой данных (таблица "Пересечения")
    public class SplitSegmentsRepository : IRepository<Segment>
    {
        public bool Add(Segment entity)
        {
            try
            {
                using (SEGMENTSEntities context = new SEGMENTSEntities())
                {
                    //Переводим сущность DataLayer в сущность БД
                    Пересечения dbSegment = new Пересечения();
                    dbSegment.X1 = entity.Start.XPos;
                    dbSegment.Y1 = entity.Start.YPos;
                    dbSegment.X2 = entity.End.XPos;
                    dbSegment.Y2 = entity.End.YPos;
                    //Сохранаем отрезок в таблице
                    context.Пересечения.Add(dbSegment);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(long id)
        {
            throw new NotImplementedException();
        }

        public Segment Select(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Segment> SelectAll()
        {
            throw new NotImplementedException();
        }

        public bool Update(Segment entity)
        {
            throw new NotImplementedException();
        }
    }
}
